% Example Eclipse Workspace with OSGi
% Christian Piechnick and Thomas Kühn
% 9.5.2018

# Installation

* Unzip the example archive `example.zip`
* Start eclipse and "Switch Workspace" to the "osgi" folder 
* Enable Automatic Build *(Project —> Build Automatically)*
* Build the workspace *(Project —> Build All)*
* The workspace has already a preconfigured run configuration “OSGi Framework”
* Use run “CRM Example” to start the `OSGi` and the console
* See the console output
* Play with the console commands[^1]

[^1]: http://www.vogella.com/tutorials/OSGi/article.html#the-osgi-console
